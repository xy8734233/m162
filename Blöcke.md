<details>
<summary> Block 2 </summary>
# Doumentation Block 2

## Datenstruckturen:

#### Die Kenntnis von Datenstrukturen ist essenziell für Applikationsentwickler. Sie müssen Daten in ihren Anwendungen laden, verarbeiten, ausgeben und speichern können. Oft geschieht dies durch den Zugriff auf eine Datenbank. Dieses Kapitel ist daher für Ihr Verständnis von Programmierung und Daten von großer Bedeutung.

## Datensatz / Record / Tupel:

#### Datensätze, auch als "Tupel" oder auf Englisch als "Records" bezeichnet, sind grundlegende Datenstrukturen. Sie enthalten Werte, die andere Werte in einer festgelegten Anzahl und Reihenfolge einschließen. Die Identifizierung von Datensätzen erfolgt in der Regel durch eines oder mehrere der in ihnen enthaltenen Elemente. Sie werden häufig verwendet, um einzelne Zeilen in Tabellen oder CSV-Dateien darzustellen.

### Beispiele:

##### (ID=12, Vorname=Hans, Nachname=Wenger, Telefonnummer=07911111111, Adresse=Zuercherstrasse 12)
##### [Länge=4.5, Song=Patience, Band=Guns'n'Roses]

Ob die Klammern rund oder eckig sind, spiel hier keine Rolle. Wichtig ist, dass die Daten thematisch zusammen gehören und normalerweise mehrere Tuples des gleichen Typs existieren. Daher kann eine Tuple/Record als eine Zeile einer Tabelle betrachtet werden.

## Array:

#### Ein Array ist eine häufig verwendete Datenstruktur, die mehrere Variablen des gleichen Datentyps speichert. Der Zugriff auf einzelne Elemente erfolgt über einen Index, wodurch das Array eine Liste von Elementen darstellt, die an bestimmten Stellen abgelegt sind. Diese Felder können auch selbst Arrays sein, wodurch mehrdimensionale Arrays (z.B., zweidimensionale Arrays) entstehen. Es gibt keine strikte Begrenzung für die Anzahl der Dimensionen, die ein Array haben kann. Die meisten Programmiersprachen bieten eine eingebaute Unterstützung für Arrays als grundlegende Datenstruktur.

#### Eine Sonderform des Arrays ist das assoziative Array, oft auch als Dictionary bezeichnet. Hier erfolgt der Zugriff nicht nur über numerische Indizes, sondern auch über benutzerdefinierte Schlüssel, die auf die gespeicherten Daten verweisen. Eine gängige Implementierung eines assoziativen Arrays ist die Verwendung von Hashtabellen.

#### Arrays sind aufgrund ihrer Einfachheit und weitreichenden Anwendungsmöglichkeiten in den meisten Programmiersprachen als grundlegender Datentyp vorhanden.

### Beispiele Array:

##### [12, 45, 23, 38, 28]
##### ["Hans", "Werner", "Sabine", "Rafael", "Susanne"]
##### ["pos01" = "Position 01", "pos02" = "Position 02", ...] => Hier handelt es sich um ein assoziatives Array und darum muss der Schlüssel mit angegeben werden. Also an der Position "pos01" befindet sich der Wert "Position 01"

## Verkettete Liste:

#### Eine verkettete Liste ist eine Datenstruktur, bei der jedes Element auf das nächste verweist, um Objekte dynamisch zu speichern. Die Blockchain-Technologie verwendet eine ähnliche Verknüpfung von Elementen, um Blöcke miteinander zu verbinden.

## Stapelspeicher / Stack:

#### In einem Stapelspeicher (engl. stack oder "Kellerspeicher") kann eine beliebige Anzahl von Objekten gespeichert werden, jedoch können die gespeicherten Objekte nur in umgekehrter Reihenfolge wieder gelesen werden. Dies entspricht dem LIFO-Prinzip (Last In, First Out). Zu einem Stapelspeicher gehören zumindest die Operationen

#### push, um ein Objekt im Stapelspeicher abzulegen und
#### pop, um das zuletzt gespeicherte Objekt wieder zu lesen und vom Stapel zu entfernen.
#### Weitere Optionen können top oder peek sein, um das oberste Element zu lesen, ohne es zu löschen.

### Altagsbeispiel:

##### Einsteigen in Bus, Metro, etc. Der letzte der reingeht, muss oft als erster wieder aussteigen, damit die anderen durch können.

## Warteschlange (queue):

#### In einer Warteschlange (engl. queue) kann eine beliebige Anzahl von Objekten gespeichert werden, jedoch können die gespeicherten Objekte nur in der gleichen Reihenfolge wieder gelesen werden, wie sie gespeichert wurden. Dies entspricht dem FIFO-Prinzip (First In, First Out). Zu einer Queue gehören zumindest die Operationen

#### enqueue, um ein Objekt in der Warteschlange zu speichern und
#### dequeue, um das zuerst gespeicherte Objekt wieder zu lesen und aus der Warteschlange zu entfernen

### Altagsbeispiel:

##### Warteschlange vor Openair-Kasse, Club, Disko

## Vorwarteschlange:

#### Eine Spezialisierung der Warteschlange ist die Vorrangwarteschlange, die auch Prioritätswarteschlange bzw. engl. Priority Queue genannt wird. Dabei wird vom FIFO-Prinzip abgewichen. Die Durchführung der enqueue-Operation, die in diesem Fall auch insert-Operation genannt wird, sortiert das Objekt gemäß einer gegebenen Priorität, die jedes Objekt mit sich führt, in die Vorrangwarteschlange ein. Die dequeue-Operation liefert immer das Objekt mit der höchsten Priorität. Vorrangwarteschlangen werden meist mit Heaps implementiert.

### Altagsbeispiel:

##### Warteschlange vor den angesagten Club und du stehst auf der VIP-Liste oder kennst den Türsteher

 ## Graph:

 #### Ein Graph ermöglicht es als Datenstruktur die Unidirektionalität der Verknüpfung zu überwinden und können Referenzen auf mehrere Objekte enthalten. Die Operationen sind auch hier das Einfügen, Löschen und Finden eines Objekts.
 [Studyfix](https://studyflix.de/informatik/grundbegriffe-der-graphentheorie-1285)

### Altagsbeispiel:

##### Routenberechnung in einem GPS-Navigationssystem nach dem Dijkstra-Algorithmus.

## Baum:

#### Bäume sind spezielle Formen von Graphen in der Graphentheorie. Bäume besitzen nur eine eingehende Verknüpfung, aber mehrere ausgehende Verknüpfungen. Eine Spezialform ist der Binärbäumen, bei dem die Anzahl der Kinder höchstens zwei ist und in höhen-balancierten Bäumen angelegt wird, bei dem sich die Höhen des linken und rechten Teilbaums an jedem Knoten nicht zu sehr unterscheiden. Bei geordneten Bäumen, insbesondere Suchbäumen, sind die Elemente in der Baumstruktur geordnet abgelegt, sodass man schnell Elemente im Baum finden kann.
[Studyfix Baum (Binärbaum)](https://studyflix.de/informatik/binarbaum-1362)

</details>
<details>
<summary>Block 3 </summary>

#A1G 
## 31.08.2023


| Begriff                  | Bedeutung                               |
| ------------------------ | --------------------------------------- |
| A  Redundanz             | Wiederholte oder überflüssige Daten    |
| B  Dateityp              | Spezifische Art der Datei oder Daten    |
| C  Inkonsistenz          | Widersprüchliche oder unvollständige Daten |
| D  Nachricht             | Mitteilung oder Information             |
| E   Primärschlüssel       | Eindeutige Identifikation in einer Tabelle |
| F   Entitätsmenge/Tabelle | Strukturierte Datenorganisation         |
| G  Aktualität            | Aktueller Zustand oder Gültigkeit       |
| H  Attribut              | Eigenschaft oder Merkmal                |
| I   Datenbeziehung       | Verbindung zwischen Daten                |
| J   Datentyp             | Art oder Format der Daten               |


### 1. Frage
#### Dieses Element hat genau einen Namen und willkürlich angeordnete Entitäten?

### Antwort:
#### F Die Daten sind strukturiert organisiert.

### 2. Frage
#### Jede in der Schweiz geborenen Person ist genau eine AHV Nummer zugeordnet.

### Antwort:
#### I Datenbeziehung

### 3. Frage
#### Die Daten sind strukturiert organisiert.

### Antwort
#### F Entitätsmenge/Tabelle

### 4. Frage
#### INTEGER oder TEXT nennt man in der Datenbanktechnik einen ...

### Antwort
#### J Datentyp

### 5. Frage
#### Eigenschaft von neuen und eventuell wertvollen Informationen.

### Antwort
#### G Aktualität

### 6. Frage
#### Ein Bild z.B. ist auf einer Festplatte auf eine bestimmte Weise abgespeichert.

### Antwort
#### B  Dateityp

### 7. Frage
#### Eine Entitätsmenge hat üblicherweise mehrere davon.

### Antwort
#### H Attribut

### 8. Frage
#### Jede Zeile innerhalb einer Tabelle wird durch einen Attributwert identifiziert.

### Antwort
#### E Primärschlüssel

### 9.Frage
#### Jede Tabelle kann genau einen ... besitzen.

### Antwort
#### E Primärschlüssel

### 10. Frage
#### Newsportale berichten über dasselbe Ereignis, das für Sie keinen Neuigkeitswert mehr hat.

### Antwort
#### A Redundanz

### 11. Frage
#### In einer Datenbank sind die Lottozahlen dieser Woche unvollständig gespeichert.

### Antwort
#### C Inkonsistenz

### 12. Frage
#### Meldungen, die auf eine Sache oder ein Ereignis aufmerksam machen.

### Antwort
#### D Nachricht

### 13. Frage
#### Wählen Sie die korrekte Antwort aus. 
#### Was ist ein Entitätswert?
#### Abzug bei falscher Antwort.
#### Frage 13Wählen Sie eine Antwort:

### Antwort
#### Eine Zelle

### 14. Frage
#### Wählen Sie die korrekte Antwort aus. 
#### Was ist der „Vorname“ in einer Kundentabelle?
#### Abzug bei falscher Antwort.

### Antwort
#### Ein Attribut
</details>
<details>
<summary> Block 4 </summary>

## Repetition - Daten charakterisieren

### 1. Frage
#### Die vertikale Achse des kartesischen Koordinatensystems heisst...
### Antwort
#### F Ordinate
### 2. Frage
#### Dass die Schweiz bald die 8 Millionen Einwohnergrenze erreicht ist ausgedrückt in...
### Antwort
#### D Liniendiagramm
### 3. Frage
#### Alle Zeilen einer Tabelle werden durch diesen eindeutig identifiziert...
### Antwort
#### A Primärschlüssel
### 4. Frage
#### Neben dem Tabellennamen werden auch die Spalten eindeutig benannt.
### Antwort
#### H Attributnamen
### 5. Frage
#### Stellt Segmente in Vergleich zu einer Gesamtmenge.
### Antwort
#### J Kreisdiagramm
### 6. Frage
#### Der Zelleninhalt einer Tabelle heisst...
### Antwort
#### C Datenwert
### 7. Frage
#### Eine Tabelle ist eine Menge von ...
### Antwort
#### I Entitäten
### 8. Frage
#### Braucht neben dem Liniendiagramm auch das kartesische Koordinatensystem.
### Antwort
#### D Liniendiagramm
### 9. Frage
#### Den täglichen Kursverlauf einer Aktie verfolgt man am besten mit einem ...
### Antwort
#### D Liniendiagramm
### 10. Frage
#### … sind unstrukturierte Daten.
### Antwort
#### E Dokumente


</details>